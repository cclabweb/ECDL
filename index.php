<?php /*

C=candidati
E=esami
U=utenti
D=sessioni
K=skillscard
S=suite

C.abilitato = 0 amministratore, 1 interno, 2 esterno, 3 sospeso
K.abilitato = 0 disponibile, 1 interna, 2 esterna, 3 sospesa
S.abilitato = 0 standard, 1 profile, 2 update, 3 outdated
E.stato = 0 prenotato, 1 non superato, 2 superato, 3 annullato

*/
include 'config.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $CFG_Title; ?></title>
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/bootstrap-responsive.css" rel="stylesheet">
	<script src="js/jquery-3.3.1.slim.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<style>
		#menu select{margin-bottom:-4px;padding-bottom:-4px;font-weight:bold;}#menu a{font-weight:bold;}
		.row{margin:0 !important;}
		div.ecdl-modulo{float:left;border:0 solid #f00;width:18em;height:18em;margin:0.2em;padding:0 0.6em 0.4em 0.6em;text-align:center;background:no-repeat center/100% url('img/badge-3.png');}
		label.ecdl-control-label{margin-bottom:0.6em;}
		div.ecdl-controls-group{height:80%;vertical-align:middle;border:0 solid #00f;}
		div.ecdl-control-suite{border:0 solid #0f0;}
		div.ecdl-control-sessioni{border:0 solid #f0f;}
		.btn, .btn-primary, .btn-success, .btn-warning, .btn-danger, .btn-info, .btn-inverse{font-size:1.2em;}
	</style>
</head>
<body>
	<?php // object id="embeddedhtml" type="text/html" data="https://www.canossacampus.it/moodle/campus/ecdl.php" style="border: medium none; overflow-y: scroll; height: 1234px;" onload="this.style.height=this.contentDocument.body.scrollHeight +'px';" width="98%"></object ?>
	<div class="container-fluid">
		<?php
		// NOTA BENE introdurre qui il login...
		$usr = (isset($USER->username) ? $USER->username : "");
		// NOTA BENE fatto il login uso lo username nel db.Sqlite per ricavare: codicefiscale, nome, cognome, abilitazione ed eventuale skillscard
		if ( $usr == '' ) {
			break;
		}
		$sql = "SELECT candidati.username, skillscard.codicefiscale, candidati.nome, candidati.cognome, candidati.abilitato, skillscard.skillscard, skillscard.tipo
				  FROM candidati
			INNER JOIN skillscard ON candidati.codicefiscale = skillscard.codicefiscale
				 WHERE candidati.username = '".$usr."'
			  ORDER BY skillscard.skillscard DESC";
		$query = $db->query($sql);
		$row = $query->fetchArray();
		$ucf = $row['codicefiscale'];
		$usn = $row['nome']." ".$row['cognome'];
		$usl = $row['abilitato'];
		$usk = $row['skillscard'];
		$gcf = (isset($_GET['codicefiscale']) ? $_GET['codicefiscale'] : $ucf);
		$gsk = (isset($_GET['skillscard']) ? $_GET['skillscard'] : $usk);
		$sql = "SELECT candidati.username, skillscard.codicefiscale, candidati.nome, candidati.cognome, candidati.abilitato, skillscard.skillscard, skillscard.tipo
				  FROM candidati
			INNER JOIN skillscard ON candidati.codicefiscale = skillscard.codicefiscale
				 WHERE skillscard.codicefiscale ='".$gcf."' OR skillscard.skillscard='".$gsk."'
			  ORDER BY skillscard.skillscard DESC";
		$query = $db->query($sql);
		$row = $query->fetchArray();
		$gcf = $row['codicefiscale'];
		$gsk = $row['skillscard'];
		$gnc = $row['nome']." ".$row['cognome'];
		// ######################################## HEADER, Start ########################################

		if ($usl <= 2) {
			// ==================== HEADER MENU (Table = Null, Action = Null) ====================
			if ($usl == 0 && ($_GET['action']=='' || $_GET['action']=='read')) { ?>

				<div class="row">
					<h5><?php echo $CFG_Title." | Anno ".date('Y')."-".(date('Y')+1).", ".$usn."</h5><div><strong>Database</strong> | Username: ".$usr." | Codice Fiscale: ".$ucf." | Abilitazione: ".$usl." | SkillsCard: ".$usk."<br/><strong>Browser</strong> | URL: ".$_SERVER['HTTP_REFERER']."<br/>".
					"<strong>Form</strong> | Table: ".$_GET['table']." | Action: ".$_GET['action']." | Data: ".$_GET['data']." | Codice Fiscale: ".$_GET['codicefiscale']." | SkillsCard: ".$_GET['skillscard']; ?><br/>&nbsp;</div>
				</div>

				<div class="container-fluid">
				<div id="menu" class="row" style="background-color:#ccc !important;padding:4px;">
					<form method="POST" name="scelta">
					<div class="form-row">

						<?php // ########## MENU (Query = Candidati) ########## ?>
						<div class="col" style="float:left;margin-right:8px;">
							<select id="candidati_filtri" onchange="candidato_scelta()" class="form-control" style="width:100px;float:left;">
							<option>- Tutti</option>
							<option>- Interni</option>
							<option>- Esterni</option>
							<option>- Sospesi</option>
							<option selected disabled>Candidati...</option>
							<?php
							$sql = "SELECT rowid, codicefiscale, cognome, nome
									  FROM 'candidati'
									 WHERE codicefiscale != ''
								  ORDER BY cognome ASC, nome ASC";
							$query = $db->query($sql);
							while($row = $query->fetchArray()) {
								echo "<option value='".$row['codicefiscale']."'>".$row['cognome']." ".$row['nome']."</option>";
							} ?>
							</select><script>function candidato_scelta(){ location.href="index.php?table=candidati&action=read&codicefiscale=" + document.getElementById("candidati_filtri").value; }</script>
						</div>

						<?php // ########## MENU (Query = SkillsCard) ########## ?>
						<div class="col" style="float:left;margin-right:8px;">
							<select id="skillscard_filtri" onchange="skillscard_scelta()" class="form-control" style="width:100px;float:left;">
							<option>- Tutte</option>
							<option>- Scadute</option>
							<option>- Disponibili</option>
							<option>- Nuova ECDL</option>
							<option>- ECDL Core</option>
							<option selected disabled>SkillsCard...</option>
							<?php
							$sql = "SELECT rowid, * FROM view_skillscard-abilitate";
							$sql = "SELECT rowid, codicefiscale, skillscard, tipo, abilitato 
									  FROM 'skillscard' 
									 WHERE (abilitato = '1' OR abilitato = '2')
								  ORDER BY skillscard ASC";
							$query = $db->query($sql);
							while($row = $query->fetchArray()) {
								echo "<option value='".$row['codicefiscale']."_".$row['skillscard']."'>".$row['skillscard']." ".$row['tipo']."</option>";
							}
							 ?>
							</select><script>function skillscard_scelta(){ location.href="index.php?table=skillscard&action=read&codicefiscale=" + document.getElementById("skillscard_filtri").value.substr(0, 16) + "&skillscard=" + document.getElementById("skillscard_filtri").value.substr(17, 16); }</script>
						</div>

						<?php // ########## MENU (Query = Sessioni) ########## ?>
						<div class="col" style="float:left;margin-right:8px;">
							<select id="sessioni_filtri" onchange="sessioni_scelta()" class="form-control" style="width:100px;float:left;">
							<option>- Tutte</option>
							<option>- Future</option>
							<option>- Passate</option>
							<option selected disabled>Sessioni...</option>
							<?php
							$sql = "SELECT rowid, data
									  FROM 'sessioni'
									 WHERE data >= DATETIME('now','localtime')
								  ORDER BY data ASC";
							$query = $db->query($sql);
							while($row = $query->fetchArray()) {
								echo "<option value='".$row['data']."'>".$row['data']."</option>";
							} ?>
							</select><script>function sessioni_scelta(){ location.href="index.php?table=sessioni&action=read&data=" + document.getElementById("sessioni_filtri").value; }</script>
						</div>

						<?php // ########## MENU (Table = Suite) ########## ?>
						<div class="col" style="float:left;margin-right:8px;">
							<a href="index.php?table=suite" class="btn">Suite</a>
						</div>

						<?php // ########## MENU (DB = Adminer) ########## ?>
						<div class="col" style="float:left;margin-right:8px;">
							<a href="db/adminer-4.2.1.php?sqlite=&username=&db=ecdl.db" class="btn">DB</a>
						</div>
					</div>
					</form>
				</div>
				</div>
				<?php
			}
			// ######################################## TABLES, End ########################################

			// ######################################## TABLES, Start ########################################

			if ($gsk !== '') {
				$sql0 = "SELECT rowid, data
						   FROM 'sessioni'
						  WHERE data >= DATETIME('now','localtime')
					   ORDER BY data ASC";
				$query0 = $db->query($sql0);
				$sessioni_date = "<select class='btn-warning' style='text-align:left !important;font-weight:bold;width:180px;'><option value='0'>Scegliere una data...</option>";
				while($row0 = $query0->fetchArray()) {
					$sessioni_date = $sessioni_date."<option value='".$row0['data']."'>".$row0['data']." ven, 14-18 ca.</option>";
				}
				$sessioni_date = $sessioni_date."</select>";
				$sql1 = "SELECT rowid, modulo AS modulo_nome
						   FROM suite
						  WHERE (abilitato = '1' OR abilitato = '2')
					   GROUP BY modulo
					   ORDER BY abilitato ASC, suite ASC;";
				// Variante per un elenco di tutti i Moduli disponibili per una singola SkillsCard con indicazione di quelli superati e non...
				$sql1 = "SELECT s.rowid, s.modulo, s.abilitato, e.codice, e.data, e.skillscard, e.versione, e.lingua, e.suite, e.percentuale
						   FROM (SELECT * FROM suite WHERE (abilitato = '1' OR abilitato = '2') GROUP BY modulo) AS s
				LEFT OUTER JOIN (SELECT * FROM esami JOIN sessioni WHERE esami.skillscard='".$gsk."' AND sessioni.codice=esami.codice ORDER BY percentuale DESC) AS e
							 ON s.modulo=e.modulo
					   ORDER BY s.abilitato ASC";
				$tit = $gnc.", SkillsCard ".$gsk." | Situazione e Prenotazione Esami...";
				echo "<div class='ecdl-row'><h3>".$tit."</h3></div>"; ?>
				<form method="POST" class="form-horizontal" name="prenotazioni">
					<?php
					$mop = ""; // Modulo precedente, nome
					$mes = 0;  // Modulo precedente, esito
					$cer = 1;
					$min = "75,00%";
					$query1 = $db->query($sql1);
					while($row1 = $query1->fetchArray()) {
						if ($mop != $row1['modulo']) {
							echo ($mop == '' ? "" : "</div></div>")."<div class='ecdl-modulo ecdl-mod".$mop."'>";
							echo "<label for='modulo' class='ecdl-control-label'>".($mop == $row1['modulo'] ? "&nbsp;" : "<div class='btn btn-primary' style='margin-left:12px;font-size:1.2em;'><strong>".$row1['modulo']."</strong></div>")."</label>";
							echo "<div class='ecdl-controls-group'>";
							if ($mop != '' && $mes == 0) {
								echo "<div class='ecdl-controls'>";
								echo "<div class='ecdl-control-suite'><select class='btn-info' style='margin:0 !important;font-weight:bold;'>";
								$sql2 = "SELECT rowid, (versione || ' ' || lingua || ' | ' || suite) AS modulo_suite
										   FROM suite
										  WHERE (abilitato='1' OR abilitato='2') AND modulo='".$mop."'
									   ORDER BY versione DESC, suite ASC;";
								$query2 = $db->query($sql2);
								while($row2 = $query2->fetchArray()) {
									echo "<option value='".$row2['rowid']."'>".$row2['modulo_suite']."</option>";
								}
								// Modificare HREF per salvare prenotazione in esami !!!
								echo "</select></div><div class='ecdl-control-sessioni'>
									".$sessioni_date."
									</div></div>";
								$cer = 0;
							}
						}
						$mes = ($mop != $row1['modulo'] ? 0 : $mes);
						$mes = ($row1['percentuale'] < $min ? $mes : 1);
						echo "<div class='ecdl-controls'>";
						if ($row1['codice']>0) {
							echo "<div class='ecdl-control-suite' style='padding-left:12px;'>".$row1['versione']." ".$row1['lingua']." | ".$row1['suite']."</div>";
							echo "<div class='ecdl-control-sessioni'>".($row1['percentuale'] < $min ? "<div class='btn btn-danger'><i class='icon-repeat icon-white'>" : "<div class='btn btn-success'><i class='icon-ok icon-white'>")."</i>
								".$row1['data']." | ".$row1['percentuale']."
								</div></div></div>".($mop == $row1['modulo'] ? "" : "");
						} else {
							$cer = 0;
							echo "<div class='ecdl-control-suite'><select class='btn-info' style='margin:0 !important;font-weight:bold;'>";
							$sql2 = "SELECT rowid, (versione || ' ' || lingua || ' | ' || suite) AS modulo_suite
									   FROM suite
									  WHERE (abilitato='1' OR abilitato='2') AND modulo='".$row1['modulo']."'
								   ORDER BY versione DESC, suite ASC;";
							$query2 = $db->query($sql2);
							while($row2 = $query2->fetchArray()) {
								echo "<option value='".$row2['rowid']."'>".$row2['modulo_suite']."</option>";
							}
							// Modificare HREF per salvare prenotazione in esami !!!
							echo "</select></div><div class='ecdl-control-sessioni'>
								".$sessioni_date."
								</div></div>";
							$mes = 1;
						}
						$mop = $row1['modulo'];
					}
					echo "</div></div></div>".($usl <= 1 ? "<div style='margin-top:23px;'><a href='index.php?table=esami&action=read&skillscard=".$gsk."&modulo=".$mop."' class='btn btn-primary' style='font-weight:bold;width:100%;font-size:1.4em;'><i class='icon-edit icon-white'></i> Scegliere Suite e Date per Prenotare gli Esami e cliccare qui</a></div>" : "");
					?>
				</form><?php
			}

			// ======================================== Table = CANDIDATI/SKILLSCARD ========================================
			if ($usr <= 1 || $_GET['table']=='candidati' || $_GET['table']=='skillscard') {

				// ########## Table = CANDIDATI/SKILLSCARD, Action = READ ##########
				$scelta = ($usl == 0 ? $gcf : $ucf);
				if ($scelta != '' && ($_GET['action']=='' || $_GET['action']=='read')) {
					switch ($scelta) {
						// -------------------- Table, Candidati
						case "- Tutti":
							$tit = " dei Candidati";
							$sql = "SELECT K.rowid AS rowid, C.cognome, C.nome, C.codicefiscale, K.skillscard, K.tipo, K.richiesta, K.rilascio, K.abilitato AS abilitato
									  FROM candidati AS C
						   LEFT OUTER JOIN skillscard AS K ON C.codicefiscale = K.codicefiscale
									 WHERE C.abilitato > '0'
								  ORDER BY C.cognome ASC, C.nome ASC";
							break;
						case "- Interni":
							$tit = " dei Candidati interni";
							$sql = "SELECT K.rowid AS rowid, C.cognome, C.nome, C.codicefiscale, K.skillscard, K.tipo, K.richiesta, K.rilascio, K.abilitato AS abilitato
									  FROM candidati AS C
						   LEFT OUTER JOIN skillscard AS K ON C.codicefiscale = K.codicefiscale
									 WHERE C.abilitato == '1'
								  ORDER BY C.cognome ASC, C.nome ASC";
							break;
						case "- Esterni":
							$tit = " dei Candidati esterni";
							$sql = "SELECT K.rowid AS rowid, C.cognome, C.nome, C.codicefiscale, K.skillscard, K.tipo, K.richiesta, K.rilascio, K.abilitato AS abilitato
									  FROM candidati AS C
						   LEFT OUTER JOIN skillscard AS K ON C.codicefiscale = K.codicefiscale
									 WHERE C.abilitato == '2'
								  ORDER BY C.cognome ASC, C.nome ASC";
							break;
						case "- Sospesi":
							$tit = " dei Candidati sospesi";
							$sql = "SELECT K.rowid AS rowid, C.cognome, C.nome, C.codicefiscale, K.skillscard, K.tipo, K.richiesta, K.rilascio, K.abilitato AS abilitato
									  FROM candidati AS C
						   LEFT OUTER JOIN skillscard AS K ON C.codicefiscale = K.codicefiscale
									 WHERE C.abilitato == '0'
								  ORDER BY C.cognome ASC, C.nome ASC";
							break;
						// -------------------- Table, SkillsCard
						case "- Tutte":
							$tit = " delle SkillsCard";
							$sql = "SELECT K.rowid AS rowid, C.cognome, C.nome, C.codicefiscale, K.skillscard, K.tipo, K.richiesta, K.rilascio, K.abilitato AS abilitato
									  FROM skillscard AS K
						   LEFT OUTER JOIN candidati AS C ON K.codicefiscale = C.codicefiscale
								  ORDER BY K.skillscard ASC";
							break;
						case "- Scadute":
							$tit = " delle SkillsCard scadute";
							$sql = "SELECT K.rowid AS rowid, C.cognome, C.nome, C.codicefiscale, K.skillscard, K.tipo, K.richiesta, K.rilascio, K.abilitato AS abilitato
									  FROM skillscard AS K
								INNER JOIN candidati AS C
									 WHERE (K.abilitato = '0' OR K.abilitato = '') AND C.codicefiscale = K.codicefiscale
								  ORDER BY K.skillscard ASC";
							break;
						case "- Disponibili":
							$tit = " delle SkillsCard disponibili";
							$sql = "SELECT K.rowid AS rowid, C.cognome, C.nome, C.codicefiscale, K.skillscard, K.tipo, K.richiesta, K.rilascio, K.abilitato AS abilitato
									  FROM skillscard AS K
						   LEFT OUTER JOIN candidati AS C ON K.codicefiscale = C.codicefiscale
									 WHERE K.abilitato = '0'
								  ORDER BY K.skillscard ASC";
							break;
						case "- Nuova ECDL":
							$tit = " delle SkillsCard Nuova ECDL";
							$sql = "SELECT K.rowid AS rowid, C.cognome, C.nome, C.codicefiscale, K.skillscard, K.tipo, K.richiesta, K.rilascio, K.abilitato AS abilitato
									  FROM skillscard AS K
								INNER JOIN candidati AS C
									 WHERE (K.abilitato = '1' OR K.abilitato = '2') AND K.tipo = 'Nuova ECDL' AND C.codicefiscale = K.codicefiscale
								  ORDER BY K.skillscard ASC";
							break;
						case "- ECDL Core":
							$tit = " delle SkillsCard ECDL Core";
							$sql = "SELECT K.rowid AS rowid, C.cognome, C.nome, C.codicefiscale, K.skillscard, K.tipo, K.richiesta, K.rilascio, K.abilitato AS abilitato
									  FROM skillscard AS K
								INNER JOIN candidati AS C
									 WHERE (K.abilitato = '1' OR K.abilitato = '2') AND K.tipo = 'ECDL Core' AND C.codicefiscale = K.codicefiscale
								  ORDER BY K.skillscard ASC";
							break;
						default:
							$sql = "SELECT rowid, *
									  FROM candidati 
									 WHERE codicefiscale='".$scelta."'";
							$query = $db->query($sql);
							$row = $query->fetchArray();
							echo "<div class='ecdl-row'><h3>Scheda di ".$row['cognome']." ".$row['nome']."</h3></div>";
							?>
							<table class="table table-striped table-bordered table-hover table-condensed">
								<tr><td><strong>Cognome</strong></td><td><?php echo $row['cognome']; ?></td><td><strong>Nome</strong></td><td><?php echo $row['nome']; ?></td></tr>
								<tr><td><strong>Data di nascita</strong></td><td><?php echo $row['nascita']; ?></td><td><strong>Luogo di nascita</strong></td><td><?php echo $row['luogo']; ?></td></tr>
								<tr><td><strong>Codice Fiscale</strong></td><td><?php echo $row['codicefiscale']; ?></td><td><strong>Privacy</strong></td><td><?php echo $row['privacy']; ?></td></tr>
								<tr><td><strong>CAP Comune</strong></td><td><?php echo $row['cap']." ".$row['comune']; ?></td><td><strong>Indirizzo</strong></td><td><?php echo $row['indirizzo']; ?></td></tr>
								<tr><td><strong>Studio</strong></td><td><?php echo $row['studio']; ?></td><td><strong>Occupazione</strong></td><td><?php echo $row['lavoro']; ?></td></tr>
								<tr><td><strong>Username</strong></td><td><?php echo $row['username']; ?></td><td><strong>EMail</strong></td><td><?php echo $row['email']; ?></td></tr>
							</table>
							<?php
							$tit = " delle SkillsCard di ".$row['cognome']." ".$row['nome'];
							$sql = "SELECT K.rowid AS rowid, C.cognome, C.nome, C.codicefiscale, K.skillscard, K.tipo, K.richiesta, K.rilascio, K.pagamento, K.importo, K.consegna, K.abilitato AS abilitato
									  FROM candidati AS C
						   LEFT OUTER JOIN skillscard AS K ON K.codicefiscale = C.codicefiscale
									 WHERE K.codicefiscale='".$scelta."'
								  ORDER BY K.skillscard ASC";
					}
					if ($usl == 0) {
						echo "<div class='ecdl-row'><h3>Elenco ".$tit."</h3></div>"; ?>
						<table class="table table-striped table-bordered table-hover table-condensed">
							<thead>
								<?php echo (substr($scelta, 0, 2)=="- " ? "<th>Nominativo</th>" : ""); ?>
								<th>SkillsCard</th>
								<th>Tipo</th>
								<th>Richiesta</th>
								<th>Rilascio</th>
								<?php if (substr($scelta, 0, 2)!=="- ") { echo "<th>Pagamento</th><th>Importo</th><th>Consegna</th>"; } ?>
								<th>Stato</th>
								<th>Azioni</th>
							</thead>
							<tbody>
								<?php
								$query = $db->query($sql);
								while($row = $query->fetchArray()) {
									echo "<tr ".( $row['abilitato'] >= 0 ? "" : "style='color:#999'" ).">
										".(substr($scelta, 0, 2)=="- " ? "<td>".$row['cognome']." ".$row['nome']."</td>" : "")."
										<td>".$row['skillscard']."</td>
										<td>".$row['tipo']."</td>
										<td>".$row['richiesta']."</td>
										<td>".$row['rilascio']."</td>";
									if (substr($scelta, 0, 2)!=="- ") { echo "<td>".$row['pagamento']."</td><td>".$row['importo']."</td><td>".$row['consegna']."</td>"; }
									echo "<td>".$row['abilitato']."</td>
										<td>";
									echo ($usl == 0 ? "<a href='index.php?table=candidati&action=update&skillscard=".$row['skillscard']."' class='btn btn-success'>Modifica</a>" : "");
									echo ((substr($scelta, 0, 2)=="- " && isset($row['codicefiscale'])) ? "<a href='index.php?table=candidati&codicefiscale=".$row['codicefiscale']."' class='btn'>Scheda</a>" : "" );
									echo "</td></tr>";
								} ?>
							</tbody>
						</table>
						<?php 
					}
					// echo ($usl == 0 ? "<a href='index.php?table=skillscard&action=create' class='btn btn-primary'><i class='icon-plus icon-white'></i> Aggiungi</a>" : "");
				}
			}

			// ======================================== Table = SESSIONI ========================================
			if ($usl == 0 && ($_GET['table']=='sessioni' || $_GET['table']=='esiti')) {

				// ########## Table = SESSIONI, Action = READ ##########
				if ( ($_GET['table']=='sessioni' || $_GET['table']=='esiti') && ($_GET['action']=='' || $_GET['action']=='read') ) {
					$scelta = $_GET['data'];
					if ($_GET['table']=='esiti') {
						echo "<div class='ecdl-row'><h3>Elenco di Candidati, Esami ed Esiti della Sessione del ".$_GET['data']."</h3></div>";
						$sql = "SELECT D.rowid, D.data, E.codice, E.skillscard, C.cognome, C.nome, E.modulo, E.versione, E.lingua, E.suite, E.percentuale, E.pagamento, E.importo, E.stato 
								  FROM esami AS E
							INNER JOIN sessioni AS D ON D.codice=E.codice 
							INNER JOIN skillscard AS K ON E.skillscard=K.skillscard 
							INNER JOIN candidati AS C ON K.codicefiscale=C.codicefiscale
								 WHERE D.data='".$_GET['data']."'
							  ORDER BY D.data DESC, C.cognome ASC, C.nome ASC, E.modulo ASC"; ?>
						<table class="table table-striped table-bordered table-hover table-condensed">
							<thead>
								<th>SkillsCard</th>
								<th>Nominativo</th>
								<th>Modulo</th>
								<th>Versione</th>
								<th>Lingua</th>
								<th>Suite</th>
								<th>Stato</th>
								<th>Data</th>
								<th>Importo</th>
								<th>Consegna</th>
							</thead>
							<tbody>
								<?php
								$query = $db->query($sql);
								while($row = $query->fetchArray()) {
									echo "<tr>
										<td>".$row['skillscard']."</td>
										<td>".$row['cognome']." ".$row['nome']."</td>
										<td>".$row['modulo']."</td>
										<td>".$row['versione']."</td>
										<td>".$row['lingua']."</td>
										<td>".$row['suite']."</td>
										<td>".$row['percentuale']."</td>
										<td>".$row['pagamento']."</td>
										<td>".$row['importo']."</td>
										<td>".$row['stato']."</td>
									</tr>";
								} ?>
							</tbody>
						</table>
						<?php
						$scelta = "- Passate";
					}
					switch ($scelta) {
						case "- Tutte":
							$tit = "Elenco delle Sessioni";
							$sql = "SELECT rowid, data, codice
									  FROM 'sessioni'
								  ORDER BY data DESC";
							break;
						case "- Future":
							$tit = "Elenco delle Sessioni future";
							$sql = "SELECT rowid, data, codice
									  FROM 'sessioni'
									 WHERE data >= DATETIME('now','localtime')
								  ORDER BY data ASC";
							break;
						case "- Passate":
							$tit = "Elenco delle Sessioni passate";
							$sql = "SELECT rowid, data, codice
									  FROM 'sessioni'
									 WHERE data < DATETIME('now','localtime')
								  ORDER BY data DESC";
							break;
						default:
							$tit = "Scheda della Sessione del ".$_GET['data'];
							$sql = "SELECT rowid, data, codice
									  FROM 'sessioni'
									 WHERE data == '".$_GET['data']."'";
					}
					echo "<div class='ecdl-row'><h3>".$tit."</h3></div>"; ?>
					<table class="table table-striped table-bordered table-hover table-condensed">
						<thead>
							<th>Data</th>
							<th>Codice</th>
						</thead>
						<tbody>
							<?php
							$query = $db->query($sql);
							while($row = $query->fetchArray()) {
								echo "<tr>
									<td>".$row['data']."</td>
									<td>".$row['codice']."</td>
									<td>";
								if ($row['data'] < date('Y-m-d')) {
									echo "<a href='index.php?table=esiti&data=".$row['data']."' class='btn'>Dettagli</a>";
								} else {
									echo ($usl == 0 ? "<a href='index.php?table=sessioni&action=update&id=".$row['rowid']."' class='btn btn-success'>Modifica</a>" : "");
								}
								echo "</td>
								</tr>";
							} ?>
						</tbody>
					</table>
					<?php
					echo ( ($usl == 0 && ($scelta == '- Tutte' || $scelta == '- Future')) ? "<a href='index.php?table=sessioni&action=create' class='btn btn-primary'><i class='icon-plus icon-white'></i> Aggiungi</a>" : "" );
				}
			}

			// ======================================== Table = SUITE ========================================
			if ($_GET['table']=='suite') {

				// ########## Table = SUITE, Action = CREATE ##########
				if ($usl == 0 && ($_GET['action']=='create' || isset($_POST['create_save'])) ) { ?>
					<div class='ecdl-row'><h3>Aggiungi una Suite</h3></div>
					<form method="POST" class="form-vertical" name="suite_crea">
						<div>
							<label for="modulo">Modulo:</label>
							<input type="text" id="modulo" name="modulo">
						</div>
						<div>
							<label for="versione">Versione:</label>
							<input type="text" id="versione" name="versione">
						</div>
						<div>
							<label for="lingua">Lingua:</label>
							<input type="text" id="lingua" name="lingua">
						</div>
						<div>
							<label for="suite">Suite:</label>
							<input type="text" id="suite" name="suite">
						</div>
						<div>
							<label for="ambiente">Ambiente:</label>
							<input type="text" id="ambiente" name="ambiente">
						</div>
						<div>
							<label for="ambiente">Abilitato:</label>
							<select type="radio" id="abilitato" name="abilitato"><option value="0" selected>No</option><option value="1">Si</option><option value="2">Update</option><option value="3">Sospeso</option></select>
						</div>
						<a href="index.php?table=suite" class="btn">Annulla</a>
						<input type="hidden" name="table" value="suite">
						<input type="submit" name="create_save" value="Salva" class="btn btn-success">
					</form>
					<?php
					if (isset($_POST['create_save'])){
						$sql = "INSERT INTO suite (modulo, versione, lingua, suite, ambiente, abilitato)
									 VALUES ('".$_POST['modulo']."', '".$_POST['versione']."', '".$_POST['lingua']."', '".$_POST['suite']."', '".$_POST['ambiente']."', '".$_POST['abilitato']."')";
						$db->exec($sql);
						header('location: index.php?table=suite');
					}
				}

				// ########## Table = SUITE, Action = READ ##########
				if (($_GET['action']!=='create' && $_GET['action']!=='update' && $_GET['action']!=='delete') || ($_GET['action']=='read') || ($_GET['action']=='')) {
					echo "<div class='ecdl-row'><h3>Elenco delle Suite</h3></div>";
					?>
					<table class="table table-striped table-bordered table-hover table-condensed">
						<thead>
							<th>Modulo</th>
							<th>Versione</th>
							<th>Lingua</th>
							<th>Suite</th>
							<th>Ambiente</th>
						</thead>
						<tbody>
							<?php
							$sql = "SELECT rowid, *
									  FROM 'suite'
								  ORDER BY abilitato ASC, ambiente ASC, modulo ASC, lingua DESC, versione DESC";
							$query = $db->query($sql);
							while($row = $query->fetchArray()) {
								echo "<tr ".( ($row['abilitato'] == 1 || $row['abilitato'] == 2) ? "" : "style='color:#999'" ).">
									<td>".$row['modulo']."</td>
									<td>".$row['versione']."</td>
									<td>".$row['lingua']."</td>
									<td>".$row['suite']."</td>
									<td>".$row['ambiente']."</td>";
									/* <td>
										<a href='index.php?table=suite&action=update&id=".$row['rowid']."' class='btn btn-success'>Modifica</a>
										<a href='index.php?table=suite&action=delete&id=".$row['rowid']."' onclick='return confirm(`Conferma eliminazione?`)' class='btn btn-danger'>Elimina</a> */
								echo "</td>
								</tr>";
							} ?>
						</tbody>
					</table>
					<?php
					echo ($usl == 0 ? "<a href='index.php?table=suite&action=create' class='btn btn-primary'><i class='icon-plus icon-white'></i> Aggiungi</a>" : "");
				}

				// ########## Table = SUITE, Action = UPDATE ##########
				if ($usl == 0 && ($_GET['action']=='update' || isset($_POST['update_save'])) && isset($_GET['id'])) {
					$sql = "SELECT rowid, *
							  FROM suite
							 WHERE rowid = '".$_GET['id']."'";
					$query = $db->query($sql);
					$row = $query->fetchArray();
					?>
					<div class='ecdl-row'><h3>Modifica la Suite</h3></div>
					<form method="POST" class="form-horizontal" name="suite_modifica">
						<div class="control-group">
							<label for="modulo" class="control-label">Modulo:</label>
							<div class="controls"><input type="text" id="modulo" name="modulo" value="<?php echo $row['modulo']; ?>"></div>
						</div>
						<div class="control-group">
							<label for="versione" class="control-label">Versione:</label>
							<div class="controls"><input type="text" id="versione" name="versione" value="<?php echo $row['versione']; ?>"></div>
						</div>
						<div class="control-group">
							<label for="lingua" class="control-label">Lingua:</label>
							<div class="controls"><input type="text" id="lingua" name="lingua" value="<?php echo $row['lingua']; ?>"></div>
						</div>
						<div class="control-group">
							<label for="suite" class="control-label">Suite:</label>
							<div class="controls"><input type="text" id="suite" name="suite" value="<?php echo $row['suite']; ?>"></div>
						</div>
						<div class="control-group">
							<label for="ambiente" class="control-label">Ambiente:</label>
							<div class="controls"><input type="text" id="ambiente" name="ambiente" value="<?php echo $row['ambiente']; ?>"></div>
						</div>
						<div class="control-group">
							<label for="abilitato" class="control-label">Abilitato:</label>
							<div class="controls">
								<div class="form-check form-check-inline">
									<input class="form-check-input" id="abilitato1" type="radio" name="abilitato" value="1" <?php echo ( $row['abilitato'] == 1 ? "checked" : "" ) ?> style="float:left;margin-right:8px;">
									<label class="form-check-label" for="abilitato1" style="padding-top:2px;">Si</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" id="abilitato2" type="radio" name="abilitato" value="0" <?php echo ( $row['abilitato'] == 0 ? "checked" : "" ) ?> style="float:left;margin-right:8px;">
									<label class="form-check-label" for="abilitato2" style="padding-top:2px;">No</label>
								</div>
							</div>
						</div>
						<a href="index.php?table=suite" class="btn">Annulla</a>
						<input type="hidden" name="table" value="suite">
						<input type="submit" name="update_save" value="Salva" class="btn btn-success">
					</form>
					<?php
					if (isset($_POST['update_save'])) {
						$modulo = $_POST['modulo'];
						$versione = $_POST['versione'];
						$lingua = $_POST['lingua'];
						$suite = $_POST['suite'];
						$ambiente = $_POST['ambiente'];
						$abilitato = $_POST['abilitato'];
						$sql = "UPDATE suite
								   SET (modulo = '$modulo', versione = '$versione', lingua = '$lingua', suite = '$suite', ambiente = '$ambiente', abilitato = '$abilitato')
								 WHERE rowid = '".$_GET['id']."'";
						$db->exec($sql);
						header('location: index.php?table=suite');
					}
				}

				// ########## Table = SUITE, Action = DELETE ##########
				if ($usl == 0 && ($_GET['action']=='delete') && isset($_GET['id'])) {
					$sql = "DELETE FROM suite
								  WHERE rowid = '".$_GET['id']."'";
					$db->query($sql);
					header('location: index.php?table=suite');
				}

			}
		}

		// ######################################## TABLES, End ########################################
		?>
	</div>
</body>
</html>
