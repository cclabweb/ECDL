# ICDL

Draft version of an integration between Moodle and a simplified web app for ICDL Test Center and candidates.


### PRESENT AND FUTURE...

Release 3.0, milestones
* formexam*admin-when-query-for-registro-if-count<=15-email-admin (?)
* admin-Scheda-modal-edit->Pagamento-Importo->email-conferma-iscritto/a (!)
* admin-Registro-modal-edit->Stato-certificato->email-consegna-candidato/a (!)
* create-webapp-manifest-for-mobile-device (!)

Release 3.5, milestones
* hide parents/tutor data when over 18 years
* admin-confirm-email-to-secretary-and-administration-with-fiscal-data-and-attachments (!)
* self-confirmation-when-3-booked-and-disable-other-booked-with-same-skillscard-and-module (?)
* show-link-to-users-or-tutor-for-esiti-or-certificati-like-foto0123 (.)

Release 4.0, milestones
+ self/online-payment (?)

### PAST

2021, Release 3.0

2021-08-09
* admin-modal-IMPORT-CSV-SKILLSCARD*consegna-email-credenziali (ods)
* admin-modal-IMPORT-CSV-ESAMI/ESITI*unique-skillscard-sessione (ods)
* email credentials to known user when s/he ask for register
* add admin/modal upload for p-foto, p-docn, e-card and 

2021-08-08
* subscribe to write less data
* annulla/cancella firma
* lista moduli senza syllabus
* prenotazione con syllabus da array (campo con valori)
* raccolta testi in odt/PDF
* se email1 tracciare PDF e letture item accordiin
* PDF tracciati con filename
* permessi su img/PDF/DB?!

2021-08-07, fixed and added
+ admin upload foto123
+ admin modal/calc SQL import
+ subscribe to write less data
+ annulla/cancella firma
+ lista moduli senza syllabus
+ prenotazione con syllabus da array (campo con valori)
+ raccolta testi in odt/PDF
+ se email1 tracciare PDF e letture item accordiin
+ PDF tracciati con filename
+ permessi su img/PDF/DB?!

2021-08-06, fixed and added
+ modal for user document and skillscard payment
+ tooltip for exam payment
+ fixed group for user and tutor
+ improved daily report for admin

2021-08-02, fixed and added
+ save-filename-attachment-documento1-documento2-documento3
+ show-image-or-link-for-attachments
+ add-complete-PDF-for-remote-exams
+ save-update-and-retrieve-all-form-data-to-sqlite-database

2020, Release 2.0
- Temporary used Moodle Feedback to store users requests
- more details avalable in Moodle Forum on https://ocdl.it

2019, Release 1.0
- First release with PHP+SQLite integrated with Moodle
- more details available on GitLab activities of the project


## Licence
This work is released under these licenses <a href="https://creativecommons.org/licenses/by-sa/4.0/"><img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" title="CC-BY-SA 4.0"></a> that is also one way interoperable with the <a href="https://www.gnu.org/licenses/gpl-3.0.en.html"><img src="https://www.gnu.org/graphics/gplv3-88x31.png" title="GNU GPL3"></a>
